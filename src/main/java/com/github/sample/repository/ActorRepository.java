package com.github.sample.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.github.sample.model.Actor;

@Repository("ActorRepository")
public interface ActorRepository extends JpaRepository<Actor, Long>  {

	Optional<Actor> findById(Long id);

}
