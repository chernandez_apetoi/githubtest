package com.github.sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.github.sample.model.Event;

@Repository("EventRepository")
public interface EventRepository extends JpaRepository<Event, Long> {

	void deleteById(Long id);

}
