package com.github.sample.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.github.sample.model.Repo;

@Repository("RepoRepository")
public interface RepoRepository extends JpaRepository<Repo, Long> {

	void deleteById(Long id);

	Optional<Repo> findById(Long id);

}
