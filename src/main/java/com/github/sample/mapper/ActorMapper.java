package com.github.sample.mapper;

import java.util.ArrayList;
import java.util.List;

import com.github.sample.dto.ActorDto;
import com.github.sample.model.Actor;

public class ActorMapper {
	public static List<ActorDto> createListDtoFromEntity(List<Actor> actores) {
		List<ActorDto> actoresDto = new ArrayList<ActorDto>();
		if (actores != null) {
			for (Actor actor : actores) {
				actoresDto.add(createDtoFromEntity(actor));
			}
		}
		return actoresDto;
	}

	public static ActorDto createDtoFromEntity(Actor actor) {
		ActorDto actorDto = new ActorDto();
		if (actor != null) {
			actorDto.setId(actor.getId());
			actorDto.setLogin(actor.getLogin());
			actorDto.setAvatar(actor.getAvatar());
		}
		return actorDto;
	}

	public static Actor createEntityFromDto(ActorDto actorDto) {
		Actor actor = new Actor();
		if (actorDto != null) {
			actor.setId(actorDto.getId());
			actor.setLogin(actorDto.getLogin());
			actor.setAvatar(actorDto.getAvatar());
		}
		return actor;
	}
}
