package com.github.sample.mapper;

import java.util.ArrayList;
import java.util.List;

import com.github.sample.dto.RepoDto;
import com.github.sample.model.Repo;

public class RepoMapper {
	public static List<RepoDto> createListDtoFromEntity(List<Repo> repos) {
		List<RepoDto> reposDto = new ArrayList<RepoDto>();
		if (repos != null) {
			for (Repo repo : repos) {
				reposDto.add(createDtoFromEntity(repo));
			}
		}
		return reposDto;
	}

	public static RepoDto createDtoFromEntity(Repo repo) {
		RepoDto repoDto = new RepoDto();
		if (repo != null) {
			repoDto.setId(repo.getId());
			repoDto.setName(repo.getName());
			repoDto.setUrl(repo.getUrl());
		}
		return repoDto;
	}

	public static Repo createEntityFromDto(RepoDto repoDto) {
		Repo repo = new Repo();
		if (repoDto != null) {
			repo.setId(repoDto.getId());
			repo.setName(repoDto.getName());
			repo.setUrl(repoDto.getUrl());
		}
		return repo;
	}
}
