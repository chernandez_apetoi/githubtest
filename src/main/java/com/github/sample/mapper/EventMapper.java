package com.github.sample.mapper;

import java.util.ArrayList;
import java.util.List;

import com.github.sample.dto.EventDto;
import com.github.sample.model.Event;

public class EventMapper {
	public static List<EventDto> createListDtoFromEntity(List<Event> events) {
		List<EventDto> eventsDto = new ArrayList<EventDto>();
		if (events != null) {
			for (Event event : events) {
				eventsDto.add(createDtoFromEntity(event));
			}
		}
		return eventsDto;
	}

	public static EventDto createDtoFromEntity(Event event) {
		EventDto eventDto = new EventDto();
		if (event != null) {
			eventDto.setId(event.getId());
			eventDto.setType(event.getType());
			eventDto.setActor(event.getActor());
			eventDto.setRepo(event.getRepo());
			eventDto.setCreatedAt(event.getCreatedAt());
		}
		return eventDto;
	}

	public static Event createEntityFromDto(EventDto eventDto) {
		Event event = new Event();
		if (eventDto != null) {
			event.setId(eventDto.getId());
			event.setType(eventDto.getType());
			event.setActor(eventDto.getActor());
			event.setRepo(eventDto.getRepo());
			event.setCreatedAt(eventDto.getCreatedAt());
		}
		return event;
	}
}
