package com.github.sample.service;

import java.util.List;
import java.util.Optional;

import com.github.sample.model.Event;

public interface EventService {
	void deleteAllEvent();

	void deleteEventById(Long id);

	void createEvent(Event Event);

	 Optional<Event> getEventById(Long id);

	List<Event> getAllEvents();
}
