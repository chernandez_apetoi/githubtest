package com.github.sample.service;

import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.sample.dto.ActorDto;
import com.github.sample.exception.AppException;
import com.github.sample.exception.RecordNotFoundException;
import com.github.sample.mapper.ActorMapper;
import com.github.sample.model.Actor;
import com.github.sample.repository.ActorRepository;
import com.github.sample.util.Constants;
@Service("ActorService")
public class ActorServiceImpl implements ActorService {

	private static final Logger logger = LogManager.getLogger(ActorServiceImpl.class);
	private static String _METHOD_NAME;

	@Autowired
	private ActorRepository actorRepository;

	
	
	@Override
	public void deleteAllActor() {
		actorRepository.deleteAllInBatch();

	}

	public void deleteActorById(Long id) throws RecordNotFoundException{
		
		 Optional<Actor> actor = actorRepository.findById(id);
         
	        if(actor.isPresent())
	        {
	        	actorRepository.deleteById(id);
	        } else {
	            throw new RecordNotFoundException("No actor exist for given id");
	        }
		
	}

	@Override
	public ActorDto createActor(ActorDto actorDtoNew) throws AppException, Exception {
		ActorDto actorSaved = null;
		try {
			_METHOD_NAME = "createActor";
			final Actor entity = ActorMapper.createEntityFromDto(actorDtoNew);

			Optional<Actor> actor = Optional.of(entity);

			if (!actor.isPresent()) {
				actorSaved = ActorMapper.createDtoFromEntity(actorRepository.save(entity));
			}

		} catch (ConstraintViolationException cex) {
			throw new AppException(Constants.UNIQUE_COLUMN_EXCEPTION, new Exception());
		} catch (Exception e) {
			AppException cex = new AppException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
		return actorSaved;
	}

	@Override
	public List<ActorDto> getAllActors() throws AppException {
		try {
			_METHOD_NAME = "getAllActors";
			return ActorMapper.createListDtoFromEntity(actorRepository.findAll());
		} catch (Exception e) {
			AppException cex = new AppException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}

	}

	@Override
	public ActorDto getActorById(Long id) throws AppException{
		try {
			_METHOD_NAME = "getActorById";
			return ActorMapper.createDtoFromEntity(actorRepository.findById(id).orElse(null));
		} catch (Exception e) {
			AppException cex = new AppException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}

	public int deleteActorById(long id) throws AppException {
		try {
			_METHOD_NAME = "deleteActor";
			ActorDto deleteActor = this.getActorById(id);
			actorRepository.delete(ActorMapper.createEntityFromDto(deleteActor));
			return 1;
		} catch (Exception e) {
			AppException cex = new AppException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}
	
	public ActorDto updateActor(long id,ActorDto newActor) throws AppException {
		try {
			_METHOD_NAME = "updateRole";
			
			// Optional<EmployeeEntity> employee = repository.findById(entity.getId());
			Actor entity = ActorMapper.createEntityFromDto(newActor);
			Actor oldActor = actorRepository.findById(id).orElse(entity);
			if (oldActor != null) {
				Actor newEntity = ActorMapper.createEntityFromDto(newActor);
				newEntity.setId(oldActor.getId());
				return ActorMapper.createDtoFromEntity(actorRepository.save(newEntity));
			}
			return null;
		} catch (Exception e) {
			AppException cex = new AppException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}

	
}
