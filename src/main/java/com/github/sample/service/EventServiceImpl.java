package com.github.sample.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.sample.exception.BadResourceRequestException;
import com.github.sample.exception.NoSuchResourceFoundException;
import com.github.sample.model.Event;
import com.github.sample.repository.EventRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
@Service("EventService")
public class EventServiceImpl implements EventService{
	@Autowired
	private EventRepository eventRepository;
	
	@Override
	public void deleteAllEvent() {
		eventRepository.deleteAllInBatch();
		
	}

	@Override
	public void deleteEventById(Long id) {
		eventRepository.deleteById(id);
		
	}

	@Override
	public void createEvent(Event event) {
		 Optional<Event> existingEvent = eventRepository.findById(event.getId());

        if (existingEvent != null) {
            throw new BadResourceRequestException("Event with same id exists.");
        }

        eventRepository.save(event);
		
	}

	public Optional<Event> getEventById(Long id) {
		Optional<Event> Event = eventRepository.findById(id);

        if (Event == null) {
            throw new NoSuchResourceFoundException("No Event with given id found.");
        }

        return Event;
	}

	@Override
	public List<Event> getAllEvents() {
		return eventRepository.findAll();
	}

	
}
