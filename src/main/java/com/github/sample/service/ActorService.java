package com.github.sample.service;

import java.util.List;
import org.omg.CORBA.portable.ApplicationException;
import com.github.sample.dto.ActorDto;
import com.github.sample.exception.AppException;
import com.github.sample.exception.RecordNotFoundException;

public interface ActorService {
	void deleteAllActor();

	int deleteActorById(long id) throws RecordNotFoundException, AppException;

	ActorDto getActorById(Long id) throws AppException;

	List<ActorDto> getAllActors() throws AppException;

	ActorDto createActor(ActorDto actorDtoNew) throws ApplicationException, AppException, Exception;

}
