package com.github.sample.service;

import java.util.List;
import java.util.Optional;

import com.github.sample.model.Repo;

public interface RepoService {
	void deleteAllRepo();

	void deleteRepoById(Long id);

	void createRepo(Repo Repo);

	Optional<Repo> getRepoById(Long id);

	List<Repo> getAllRepo();
}
