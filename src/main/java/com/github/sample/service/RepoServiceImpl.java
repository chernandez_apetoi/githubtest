package com.github.sample.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.sample.exception.BadResourceRequestException;
import com.github.sample.exception.NoSuchResourceFoundException;
import com.github.sample.model.Repo;
import com.github.sample.repository.RepoRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
@Service("RepoService")
public class RepoServiceImpl implements RepoService {

	@Autowired
	private RepoRepository repoRepository;
	
	@Override
	public void deleteAllRepo() {
		repoRepository.deleteAllInBatch();
		
	}

	@Override
	public void deleteRepoById(Long id) {
		repoRepository.deleteById(id);
		
	}

	@Override
	public void createRepo(Repo repo) {
		 Optional<Repo> existingRepo = repoRepository.findById(repo.getId());

	        if (existingRepo != null) {
	            throw new BadResourceRequestException("Actor with same id exists.");
	        }

	       repoRepository.save(repo);

		
	}

	@Override
	public Optional<Repo> getRepoById(Long id) {
		Optional<Repo> repo = repoRepository.findById(id);

        if (repo == null) {
            throw new NoSuchResourceFoundException("No Event with given id found.");
        }

        return repo;
	}

	@Override
	public List<Repo> getAllRepo() {
		return repoRepository.findAll();
	}

}
