package com.github.sample.exception;

public class AppException extends Exception {
	private static final long serialVersionUID = 2884752743559448878L;

	public AppException(String cause, Exception e) {
		super(cause, e);
	}

	public AppException(String mensaje) {
		super(mensaje);
	}
}
