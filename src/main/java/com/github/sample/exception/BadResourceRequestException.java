package com.github.sample.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadResourceRequestException extends RuntimeException {
	public BadResourceRequestException(String msg) {
        super(msg);
    }
	
	private static final long serialVersionUID = 1L;

}
