package com.github.sample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.github.sample.service.ActorService;
import com.github.sample.service.EventService;
import com.github.sample.service.RepoService;

@RestController
@RequestMapping(value = "/events")
public class EventsController {

	@Autowired
	private EventService eventService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private RepoService repoService;

	@RequestMapping(value = "/erase", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void deleteAllEvents() {
		
		actorService.deleteAllActor();
		repoService.deleteAllRepo();
		eventService.deleteAllEvent();
	}
}
